let tableau = [5, 15, 26, 12, 78, 56, 45, 78, 42];
console.log(tableau);
// console.table(tableau);

// L'utilisateur entre un nombre qu'on ajoute à la fin du tableau
const regex = /^[0-9]*$/;

function new_value() {
    let value = window.prompt("Entrez un nombre :");
    if (value.match(regex)) {
        tableau.push(parseInt(value));
        console.log(tableau);
    } else {
        new_value();
    }
}
new_value();

// Inversement de deux positions dans le tableau
tableau[2] = 42;
tableau[8] = 26;
console.log(tableau);

// Affichade de la 3è et de la 7è case dans le tableau
console.log(tableau[2] + " / " + tableau[6]);

// Suppression de la 5è case : c'est-à-dire l'index 4
tableau.splice(4, 1);
console.log(tableau);

// Copie du tableau
let array_copie = tableau.slice();
console.log(`Mon tableau copié : ${array_copie}`);