// L'utilisateur entre un nombre compris entre 0 et 5
const regex1 = /^[0-5]$/;

function value_0_5() {
    let value = window.prompt("Entrez un chiffre compris entre 0 et 5 :");
    if (value.match(regex1)) {
        console.log(value);
    } else {
        value_0_5();
    }
}
value_0_5();


// Ecrire un code qui demande un nombre de départ à l’utilisateur et affiche les 10 nombres suivants. Ex si vous saisissez 42, la page affichera chaque valeur comprise entre 43 et 52.
let i;
const regex2 = /^[0-9]*$/;
function value_user() {
    let value = window.prompt("Entrez un nombre :");
    if (value.match(regex2)) {
        console.log("La valeur entrée est : " + value);
        let value_int = parseInt(value);
        for (i = value_int + 1; i < value_int + 11; i++){
            console.log(i);
        }
        multiplication(value_int);
    } else {
        value_user();
    }
}
value_user();

// Ecrire un code qui demande un nombre de départ à l’utilisateur et affiche ensuite sa table de multiplication (de x1 à x10)
function multiplication(number) {
    for (i = 1; i < 11; i++) {
        number_m = number * i;
        console.log(`${number} x ${i} = ${number_m}`);
    }
}

// Exercice rendre la monnaie
function price_article() {
    let article_price = window.prompt("Quel est le prix de l'article ?");
    if (article_price.match(regex2)) {
        console.log("Le prix de l'article est : " + article_price + " €.");
        let value_article = parseInt(article_price);
        payement(value_article);
    } else {
        price_article();
    }
}

function payement(value_article, message_error = "") {
    let paid = window.prompt("Avec quelle somme payez-vous ?" + " " + message_error);
    if (paid.match(regex2)) {
        let paid_int = parseInt(paid);

        if (paid_int >= value_article) {
            console.log("Vous payez avec " + paid_int + " €.");
            monnaie_a_rendre(value_article, paid_int);
        } else {
            payement(value_article, "Vous devez payer l'article en totatilé minimum.");
        }
    } else {
        payement(value_article, "Entrez que des chiffres.");
    }
}

function monnaie_a_rendre(value_article, paid) {
    if (paid > value_article) {
        to_return = paid - value_article;
        console.log("La caissière vous doit : " + to_return + "€.");
        let billet_10 = 0;
        let billet_5 = 0;
        let piece_2 = 0;
        let piece_1 = 0;
        while (to_return >= 10) {
            billet_10++;
            to_return -= 10;
        }
        while (to_return >= 5) {
            billet_5++;
            to_return -= 5;
        }
        while (to_return >= 2) {
            piece_2++;
            to_return -= 2;
        }
        while (to_return >= 1) {
            piece_1++;
            to_return -= 1;
        }
        console.log("Billet(s) de 10€ à rendre : " + billet_10);
        console.log("Billet(s) de 5€ à rendre : " + billet_5);
        console.log("Pièce(s) de 2€ à rendre : " + piece_2);
        console.log("Pièce(s) de 1€ à rendre : " + piece_1);
        console.log("Il reste donc : " + to_return + "€ à rendre. Le compte est bon !");
    } else {
        console.log("Bravo, vous avez payé tout pile ! Aucune monnaie à rendre.");
    }
}
price_article();