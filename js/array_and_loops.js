// Fonction qui rempli un tableau de 7 valeurs numériques 0
let i;
function array_of_zero() {
    let array = [];
    for (i =0; i < 7; i++) {
        array.push(0);
    }
    return array;
}
console.log(array_of_zero());

// Partant d’un tableau rempli avec les notes de votre choix (ex [5,10,15,17,6,18,12,7,6,2,14]), calculé et affiché la moyenne des notes de ce tableau
let array = [5,10,15,17,6,18,12,7,6,2,14];
let somme_values = 0;

function moyenne() {
    for (i = 0; i < array.length -1 ; i++) {
        somme_values += array[i];
    }
    return somme_values/array.length;
}
console.log("La moyenne des nombres contenus dans le tableau est : " + moyenne());

// Demande à l'utilisateur le nombre de valeurs qu'il veut entrer
const regex1 = /^[0-9]*$/;
const regex2 = /^-?[0-9]*$/;

function number_of_values(message_error = "") {
    let value = window.prompt("Entrez le nombre de valeurs que vous voulez entrer dans un tableau. " + message_error);
    if (value.match(regex1)) {
        console.log("Vous voulez entrer " + value + " nombres dans le tableau.");
        remplissage_array(parseInt(value));
    } else {
        number_of_values("Entrez que des nombres.");
    }
}
number_of_values();

// Fonction qui permet d'ajouter un nombre si il répond bien au regex
function new_value() {
    let value = window.prompt("Entrez un nombre.");
    if (value.match(regex2)) {
        return parseInt(value);
    } else {
        new_value();
    }
}

// On rempli le tableau avec le nombre de valeurs que l'utilisateur a entré
function remplissage_array(number_values) {
    array_empty = [];
    for (i = number_values; i >=1; i--) {
        value = new_value();
        array_empty.push(value);
    }
    console.log(array_empty);
    positive_negative(array_empty);
}

// Fonction permettant de connaitre le nombre de nombres positifs et négatifs dans un tableau
function positive_negative(array) {
    let negative = 0;
    let positive = 0;
    array.forEach(value => {
        if (value >= 0) {
            positive++;
        } else {
            negative++;
        }
    });
    console.log("Dans le tableau il y a " + positive + " nombres positifs et " + negative + " nombres négatifs.");
}

// Création d'un tableau faisant la somme des valeurs au même index de 2 tableaux
array_1 = [4, 8, 7, 9, 1, 5, 4, 6];
array_2 = [7, 6, 5, 2, 1, 3, 7, 4];

function array_sum(array_1, array_2) {
    let empty_array = [];
    for (i = 0; i < array_1.length; i++) {
        empty_array.push(array_1[i] + array_2[i]);
    }
    return empty_array;
}
console.log(array_sum(array_1, array_2));