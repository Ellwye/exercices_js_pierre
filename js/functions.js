// Ecrivez une fonction qui renvoi la somme de cinq nombre fourni en arguments
function addition(n1, n2, n3, n4, n5) {
    return n1 + n2 + n3 + n4 + n5;
}
console.log(addition(2, 3, 5, 9, 14));

// Ecrivez une fonction qui calcule la factorielle d’un nombre entier supérieur ou égale à 5
// En mathématiques, la factorielle d'un entier naturel n, notée n!, ce qui se lit soit " factorielle de n " soit " factorielle n ", 
// est le produit des nombres entiers strictement positifs inférieurs ou égaux à n.
// ex factorielle 5 (écrit aussi 5 !) donne 5*4*3*2*1 = 120
const regex1 = /^[0-9]*$/;
let i;
function factorielle(message_error = "") {
    let value = window.prompt("Entrez un nombre suppérieur ou égal à 5. " + message_error);
    if (value.match(regex1)) {
        if (value >= 5) {
            value = parseInt(value);
            let factorielle_n = value;
            for (i = 1; i < value; i++) {
                console.log(i);
                factorielle_n *= i;
            }
            console.log("La factorielle de " + value + " est : " + factorielle_n);
        } else {
            factorielle("Il y a eu un problème, réessayez.");
        }
    } else {
        factorielle("Entrez que des nombres.");
    }
}
factorielle();


// Ecrivez une fonction qui demande la saisie d'une largeur, d'une longueur et affiche la surface d’un rectangle, puis affiche la valeur du périmètre (double de la somme de la longueur et de la largeur)
// Vous utiliserez les fonctions alert(qui affiche une chaîne de caractères) et prompt (qui invite à la saisie d'une donnée), dont voici les syntaxes :
// alert(« Chaîne à afficher à l'écran »)
// variable=prompt(« Question à afficher à l'écran »)
// Fonction qui permet d'ajouter un nombre si il répond bien au regex
function largeur_rectangle(message_error = "") {
    let largeur = window.prompt("Entrez la largeur du rectangle.");
    if (largeur.match(regex1)) {
        largeur = parseInt(largeur);
    } else {
        largeur = largeur_rectangle("Entrez la largeur du rectangle. N'utilisez que des nombres.");
    }
    return largeur;
}

function longueur_rectangle(message_error = "") {
    let longueur = window.prompt("Entrez la longueur du rectangle.");
    if (longueur.match(regex1)) {
        longueur = parseInt(longueur);
    } else {
        longueur = longueur_rectangle("Entrez la longueur du rectangle. N'utilisez que des nombres.");
    }
    return longueur;
}

function surface_rectangle(largeur, longueur) {
    return largeur * longueur;
}

function perimetre_rectangle(largeur, longueur) {
    return longueur * 2 + largeur * 2;
}

let largeur = largeur_rectangle();
let longueur = longueur_rectangle();
let surface = surface_rectangle(largeur, longueur);
let perimetre = perimetre_rectangle(largeur, longueur);

alert(`Le rectangle fait ${longueur}cm de longueur, ${largeur}cm de largeur. Son périmètre est donc de ${perimetre}cm et sa surface de ${surface}cm².`);

// Ecrivez une fonction qui compte le nombre de voyelle dans une chaine de caractères et l’affiche dans une balise <p> de votre page HTML (pas d’alert ici donc). Notez qu’il est possible d’appeler une fonction dans une autre fonction.
let string = "Voici une phrase";
console.log(string.split(''));

function number_voyelles(string) {
    array_string = string.split('');
    let voyelles = ["a", "e", "i", "o", "u", "y"];
    let number_of_voyelles = 0;

    array_string.forEach(letter => {
        voyelles.forEach(voyelle => {
            if (letter == voyelle) {
                number_of_voyelles++;
            }
        });
    });

    return number_of_voyelles;
}
console.log('Dans la phrase suivante : "' + string + '" Il y a ' + number_voyelles(string) + " voyelles.");